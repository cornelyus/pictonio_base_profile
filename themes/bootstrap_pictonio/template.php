<?php

/**
 * @file
 * template.php
 */

/**
 * Bootstrap pictonio base theme wrapper function responsive images
 */
function bootstrap_pictonio_preprocess_image_style(&$vars) {
        $vars['attributes']['class'][] = 'img-responsive';
        // http://getbootstrap.com/css/#overview-responsive-images
}

function bootstrap_pictonio_preprocess_page(&$vars) {

  if (drupal_is_front_page()) {
    unset($vars['page']['content']['system_main']['default_message']);//will remove message "no front page content is created"
    drupal_set_title(''); //removes welcome message (page title)
  }
}

/**
 * [bootstrap_pictonio_preprocess_html description]
 * @param  [type] $variables [description]
 * @return [type]            [description]
 */
function bootstrap_pictonio_preprocess_html($variables) {

  if(function_exists('environment_current')){
    $variables['classes_array'][] = 'environment_current_'.environment_current();
  }
}