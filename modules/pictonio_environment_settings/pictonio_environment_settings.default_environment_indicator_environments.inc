<?php
/**
 * @file
 * pictonio_environment_settings.default_environment_indicator_environments.inc
 */

/**
 * Implements hook_default_environment_indicator_environment().
 */
function pictonio_environment_settings_default_environment_indicator_environment() {
  $export = array();

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'dev_environment';
  $environment->name = 'Dev environment';
  $environment->regexurl = '.*dev.*';
  $environment->settings = array(
    'color' => '#f65e05',
    'text_color' => '#ffffff',
    'weight' => '95',
    'position' => 'top',
    'fixed' => 1,
  );
  $export['dev_environment'] = $environment;

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'prod_environment';
  $environment->name = 'Prod environment';
  $environment->regexurl = '.*\\.com';
  $environment->settings = array(
    'color' => '#29a41a',
    'text_color' => '#ffffff',
    'weight' => '99',
    'position' => 'top',
    'fixed' => 1,
  );
  $export['prod_environment'] = $environment;

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'testing_environment';
  $environment->name = 'Testing environment';
  $environment->regexurl = '.*test.*';
  $environment->settings = array(
    'color' => '#a79b1c',
    'text_color' => '#ffffff',
    'weight' => '95',
    'position' => 'top',
    'fixed' => 1,
  );
  $export['testing_environment'] = $environment;

  return $export;
}
