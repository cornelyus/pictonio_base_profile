Feature environment settings
============================

Este módulo cria condições para ser customizável a alteração do ambiente do portal. Em conjunto com ficheiros de "settings.<ambiente>.php" estão definidas diferentes condições e variáveis para os ambientes. A alteração de ambiente pode ser feita com recurso a drush "drush env-switch <ambiente>"

Ambientes
---------

* dev

* cert

* prod