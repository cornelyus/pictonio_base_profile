##Pictono Base Profile é um ponto de partida mais rápido para desenvolver sites Drupal

###Introdução

Pictonio Base Profile tem alguns dos módulos mais utilizados pré-configurados e patches aplicados.

###Instalação

Versão mínima de drush: 7

Para usar e estando dentro da pasta com o ficheiro pictonio\_base\_profile.make

* drush make pictonio\_base\_profile.make /web/path/nome\_do\_folder\_do\_site
* cd /web/path/nome\_do\_folder\_do\_site
* drush site-install pictonio_base --yes --sites-subdir=default --db-url=mysql://*dbuser*:*dbpass*@ localhost/nome\_da\_bd --account-name=admin --account-pass=secretsecret

Para instalar com outra linguagem

* Activar o download da tradução em drupal-org-core.make
* Copiar a pasta translations que estará em profiles, para dentro de pictonio_base_profile
* Adicionar ao site install "--locale=pt-pt". Pelo menos drupal core ficará instalado na linguagem escolhida.


Para testar o novo site num browser:

* cd sites/default
* drush runserver /