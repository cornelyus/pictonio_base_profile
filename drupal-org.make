; This drupal-org.make file downloads contrib modules, themes, and 3rd party libraries.

api = 2
core = 7.x

; Patches should now all be handled by patches.make file thanks to https://bitbucket.org/davereid/drush-patchfile.

; Specify default subdirectory for projects
defaults[projects][subdir] = "contrib"

; Modules
projects[] = admin_menu
projects[] = admin_views
projects[] = advagg
projects[] = breadcrumbs_by_path
projects[] = breakpoints
projects[] = chosen
projects[] = coffee
projects[] = context
; @see https://www.drupal.org/node/2463187#comment-9779355
projects[ckeditor][patch][] = "https://www.drupal.org/files/issues/allow-custom-icon-paths-2463187-1.patch"
projects[] = ctools
projects[] = date
projects[] = devel
projects[] = diff
projects[] = ds
projects[] = ds_bootstrap_layouts
projects[] = email
projects[] = entity
projects[] = entityreference
projects[] = environment
projects[] = environment_indicator
projects[] = features
projects[] = fences
projects[] = field_group
projects[] = inline_entity_form
projects[] = jquery_update
projects[] = less
projects[] = libraries
projects[] = link
projects[linkit][version] = "2.x-dev"
projects[] = menu_block
projects[] = menu_expanded
projects[] = module_filter
projects[oembed][version] = "1.x-dev"
projects[paragraphs][version] = "1.0-rc4"
projects[] = pathauto
projects[] = picture
projects[] = r4032login
projects[] = redirect
projects[] = simplify
projects[] = strongarm
projects[] = styleguide
projects[] = token
projects[] = transliteration
projects[] = title
projects[] = views
projects[views_bootstrap][version] = "3.x-dev"
; @see https://www.drupal.org/node/2203111#comment-9991487
; projects[views_bootstrap][patch][] = "https://www.drupal.org/files/issues/views_bootstrap-thumbails-columns-per-device-size-2203111-54.patch"
projects[] = views_bulk_operations

; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.2/ckeditor_3.6.6.2.zip"
libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/1.4.2/chosen_v1.4.2.zip"
libraries[less][download][type] = "get"
libraries[less][download][url] = "https://github.com/oyejorge/less.php/archive/master.zip"
libraries[less][directory_name] = "less.php"

; Themes
projects[bootstrap][type] = "theme"
projects[bootstrap][subdir] = ""