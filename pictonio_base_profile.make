; Use this file to build a full distribution including Drupal core and the
; Bear install profile using the following command:
;
; drush make build-bear.make <target directory>

api = 2
core = 7.x

; Include the additional makes files we have created for core and contrib.
includes[] = drupal-org-core.make
includes[] = drupal-org.make

; Add Pictonio Base Profile to the full distribution build.
projects[pictonio_base][type] = profile
projects[pictonio_base][subdir] = ""
projects[pictonio_base][download][type] = git
projects[pictonio_base][download][url] = https://cornelyus@bitbucket.org/cornelyus/pictonio_base_profile.git
projects[pictonio_base][download][branch] = master